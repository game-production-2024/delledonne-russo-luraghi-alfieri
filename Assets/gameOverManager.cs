using UnityEngine;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    public GameObject restartButtonPrefab; // Prefab del pulsante restart
    private GameObject restartButtonInstance;

    void Start()
    {
        if (restartButtonPrefab == null)
        {
            Debug.LogError("Restart Button Prefab non assegnato.");
        }
    }

    public void ShowRestartButton()
    {
        restartButtonPrefab.SetActive(true);
    }

    public void RestartGame()
    {
        // Carica la scena attuale per riavviare il gioco
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }
}
