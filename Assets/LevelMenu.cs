using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelMenuManager : MonoBehaviour
{
    // Classe che definisce un pulsante di livello con ID e nome della scena
    [System.Serializable]
    public class LevelButton
    {
        public int levelID;          // ID del livello
        public Button button;        // Il pulsante UI collegato
        public string sceneName;     // Nome della scena da caricare
    }

    // Array che conterr� tutti i pulsanti per la selezione del livello
    public LevelButton[] levelButtons;

    private void Start()
    {
        // Assegna un listener a ogni pulsante per caricare la scena corrispondente
        foreach (LevelButton levelButton in levelButtons)
        {
            // Aggiungi un listener anonimo per passare l'ID e il nome della scena
            levelButton.button.onClick.AddListener(() => LoadLevel(levelButton.levelID, levelButton.sceneName));
        }
    }

    // Funzione per caricare il livello specificato in base all'ID o al nome della scena
    public void LoadLevel(int levelID, string sceneName)
    {
        Debug.Log("Caricamento livello ID: " + levelID + " Scena: " + sceneName);
        // Carica la scena corrispondente al nome dato
        SceneManager.LoadScene(sceneName);
    }
}
