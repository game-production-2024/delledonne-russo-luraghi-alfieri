using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenuUI;
    public Button resumeButton;
    public Button optionsButton;
    public Button quitButton;

    private bool isPaused = false;

    void Start()
    {
        // Associa i metodi ai bottoni
        resumeButton.onClick.AddListener(Resume);
        optionsButton.onClick.AddListener(OpenOptions);
        quitButton.onClick.AddListener(QuitGame);

        // Assicurati che il menu di pausa sia disattivato all'inizio
        pauseMenuUI.SetActive(false);

        // Assicurati che il tempo sia impostato a 1 all'inizio del gioco
        Time.timeScale = 1f;

        Debug.Log("Il menu di pausa � pronto. Tempo di gioco attuale: " + Time.timeScale);
    }

    void Update()
    {
        // Controlla se il tasto ESC � stato premuto
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Tasto Escape premuto"); // Verifica che il tasto sia rilevato
            if (isPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
        else
        {
            
        }
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
        Debug.Log("Gioco ripreso"); // Conferma che il gioco � ripreso
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
        Debug.Log("Gioco in pausa"); // Conferma che il gioco � in pausa
    }

    public void OpenOptions()
    {
        Debug.Log("Opzioni aperte");
    }

    public void QuitGame()
    {
        SceneManager.LoadSceneAsync(0);
        Debug.Log("Gioco chiuso");
    }
}
