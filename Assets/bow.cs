using UnityEngine;
using System.Collections;

public class Bow : MonoBehaviour
{
    AudioManager audioManager;
    public Transform firePoint; // Punto di origine del proiettile
    public GameObject projectilePrefab; // Prefab del proiettile
    public KeyCode aimKey = KeyCode.Mouse1; // Tasto per attivare la mira (modificabile nell'Inspector)
    public KeyCode shootKey = KeyCode.Mouse0;
    public float minChargeTime = 0.1f; // Tempo minimo di caricamento
    public float maxChargeTime = 2f; // Tempo massimo di caricamento
    public float minProjectileSpeed = 5f; // Velocit� minima del proiettile
    public float maxProjectileSpeed = 20f; // Velocit� massima del proiettile
    public float shootCooldown = 1f; // Tempo di cooldown dopo ogni tiro
    public float minAimTime = 1f; // Tempo minimo di mira prima di poter sparare
    public float shootDelay = 0.2f; // Ritardo prima che la freccia venga effettivamente sparata dopo aver rilasciato il tasto di sparo
    public float recoilForce = 100f; // Forza di rinculo applicata al personaggio

    private Camera mainCamera;
    private float chargeTime;
    private bool isAiming;
    private bool canShoot = true; // Variabile per gestire il cooldown
    private Quaternion initialRotation; // Rotazione iniziale dell'arma
    private Transform armTransform; // Riferimento all'oggetto che simula il braccio

    void Start()
    {
        audioManager = GameObject.FindGameObjectWithTag("audio").GetComponent<AudioManager>();
        mainCamera = Camera.main;
        armTransform = firePoint.parent; // Presume che firePoint sia un figlio del braccio
        initialRotation = transform.localRotation; // Salva la rotazione iniziale locale
    }

    void Update()
    {
        if (isAiming)
        {
            Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            Vector3 direction = mousePosition - transform.parent.position;

            // Calcola l'angolo e ruota il braccio
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            armTransform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));

            // Flip orizzontale in base alla direzione del cursore rispetto al personaggio
            if (direction.x > 0)
            {
                FlipHorizontal(false); // Non flippa se la direzione � verso destra
            }
            else if (direction.x < 0)
            {
                FlipHorizontal(true); // Flippa se la direzione � verso sinistra
            }
        }

        if (Input.GetKeyDown(shootKey))
        {
            StartAiming();
        }
        if (Input.GetKey(shootKey))
        {
            Aim();
        }
        if (Input.GetKeyUp(shootKey))
        {
            StopAiming();
        }
        if (Input.GetKey(shootKey) && canShoot)
        {
            ChargeAiming();
        }
        if (Input.GetKeyUp(shootKey) && canShoot)
        {
            Shoot();
            audioManager.PlaySFX(audioManager.bowShooting);
        }

        // Flip orizzontale solo se non si sta mirando
        if (!isAiming)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                FlipHorizontal(true);
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                FlipHorizontal(false);
            }
        }
    }

    void Aim()
    {
        if (isAiming)
        {
            Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            Vector3 direction = mousePosition - transform.parent.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            armTransform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        }
    }

    void StartAiming()
    {
        isAiming = true;
        chargeTime = 0f;
    }

    void StopAiming()
    {
        isAiming = false;
        transform.localRotation = initialRotation; // Ripristina la rotazione iniziale
    }

    void ChargeAiming()
    {
        chargeTime += Time.deltaTime;
    }

    void Shoot()
    {
        float clampedChargeTime = Mathf.Clamp(chargeTime, minChargeTime, maxChargeTime);
        float chargeRatio = clampedChargeTime / maxChargeTime;
        float projectileSpeed = Mathf.Lerp(minProjectileSpeed, maxProjectileSpeed, chargeRatio);
        GameObject projectile = Instantiate(projectilePrefab, firePoint.position, firePoint.rotation);
        projectile.transform.localScale = projectilePrefab.transform.localScale; // Imposta la scala corretta
        Projectile projectileScript = projectile.GetComponent<Projectile>();
        chargeTime = 0;
        if (projectileScript != null)
        {
            projectileScript.isShot = true; // Imposta la variabile isShot su true
            projectileScript.speed = projectileSpeed; // Imposta la velocit� del proiettile
        }

        StartCoroutine(ShootCooldown()); // Inizia il cooldown
    }

    IEnumerator ShootCooldown()
    {
        canShoot = false; // Impedisce di sparare
        yield return new WaitForSeconds(shootCooldown); // Aspetta per il tempo del cooldown
        canShoot = true; // Permette di sparare di nuovo
    }

    void FlipHorizontal(bool flip)
    {
        if (flip)
        {
            transform.parent.localScale = new Vector3(-1, transform.parent.localScale.y, transform.parent.localScale.z);
        }
        else
        {
            transform.parent.localScale = new Vector3(1, transform.parent.localScale.y, transform.parent.localScale.z);
        }
    }
}
