
using UnityEngine;

public class enemyHealthPoints : MonoBehaviour
{
    AudioManager audioManager;
    // Variabile pubblica per i punti vita
    public int healthPoints = 3;
    public ParticleSystem enemyHit;
    private enemyManager enemyManager;

    // Variabile pubblica per il tag degli oggetti che riducono i punti vita
    public string damagingTag;
    public string damagingTag2;
    public string damagingTag3;
    public GameObject objectToDestroy;
    

    private void Start()
    {
        enemyManager = GameObject.FindObjectOfType<enemyManager>();
        audioManager = GameObject.FindGameObjectWithTag("audio").GetComponent<AudioManager>();
    }

    private void Update()
    {
        
        if (healthPoints <= 0)
        {
            // Distrugge l'oggetto
            Destroy(gameObject);
            
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        // Controlla se l'oggetto che ha colliso ha il tag specificato
        if (collision.gameObject.tag == damagingTag2 || collision.gameObject.tag == damagingTag3)
        {
            audioManager.PlaySFX(audioManager.miniRobotHurt);
            // Riduce i punti vita
            healthPoints -= 2; // Puoi modificare il valore come preferisci
            Debug.Log("chemale!!");
            createEnemyHitParticle();

            // Controlla se i punti vita sono uguali o minori di zero

        }
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Box"))
           {
            if (collision.gameObject.CompareTag("Muro"))
                {
                healthPoints = 0;
                createEnemyHitParticle();
                audioManager.PlaySFX(audioManager.miniRobotHurt);
            }
           }
        if (collision.gameObject.tag == damagingTag)
        {
            // Riduce i punti vita
            healthPoints -= 1; // Puoi modificare il valore come preferisci
            Debug.Log("chemale!!");
            audioManager.PlaySFX(audioManager.miniRobotHurt);
            createEnemyHitParticle();
        }
    }

    private void OnDestroy()
    {
        Destroy(objectToDestroy);
        audioManager.PlaySFX(audioManager.miniRobotHurt);

    }
    void createEnemyHitParticle()
    {
        enemyHit.Play();
    }
}