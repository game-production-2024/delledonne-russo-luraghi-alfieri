using UnityEngine;

public class MoveOnMeleeAttack : MonoBehaviour
{
    public float moveSpeed = 5f;  // Velocit� di movimento regolabile nell'Inspector
    AudioManager audioManager;
    private float locktimer = 1f;
    public Vector2 pushForce = new Vector2(1, 0);
    public Vector2 pushForceSX = new Vector2(-1, 0);
    private bool isTouchingNothing = false;
    public GameObject objectToShow;

    private Rigidbody2D rb;

    private void Start()
    {
        audioManager = GameObject.FindGameObjectWithTag("audio").GetComponent<AudioManager>();
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        isTouchingNothing = false;
        if (collision.CompareTag("meleeAttack"))
        {
            audioManager.PlaySFX(audioManager.boxMoving);
            rb.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
            locktimer = 0;

            // Determina la direzione del movimento in base alla posizione dell'attacco
            if (collision.transform.position.x < transform.position.x)
            {
                rb.AddForce(pushForce, ForceMode2D.Impulse);// Attacco da sinistra, muoversi a destra
            }
            else if (collision.transform.position.x > transform.position.x)
            {
                rb.AddForce(pushForceSX, ForceMode2D.Impulse);
            }
            
        }
        else
        {
            
            if (locktimer >= 1)
            {
                rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
            }
            
        }

    }
    void OnCollisionExit2D(Collision2D collision)
    {
        // Quando esce dal contatto con qualcosa, controlla se non ci sono altri contatti
        if (rb.IsTouchingLayers() == false)
        {
            locktimer = 1;
            isTouchingNothing = true;
        }
    }
    private void Update()
    {
        locktimer += Time.deltaTime;
        if (isTouchingNothing == true)
        {
            rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
        }
        if (locktimer >= 1)
        {
            rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
        }
        if (rb.velocity.y < -1)
            {
            objectToShow.SetActive(true);
        }
        else
        {
            objectToShow.SetActive(false);
        }
    }
}