using UnityEngine;

public class HandTracking : MonoBehaviour
{
    public Transform target; // Il riferimento al protagonista 'Gino'
    public float rotationSpeed = 5f; // Velocit� di rotazione, puoi regolare questo valore nel Inspector

    private Transform handBone; // Riferimento all'osso della mano sinistra

    void Start()
    {
        // Trova l'osso della mano sinistra (questo script dovrebbe essere applicato all'osso della mano sinistra)
        handBone = transform;

        if (target == null)
        {
            Debug.LogError("Target non assegnato nel HandTracking script.");
        }
    }

    void Update()
    {
        if (target != null)
        {
            // Calcola la direzione verso il target
            Vector2 direction = (target.position - handBone.position).normalized;

            // Calcola l'angolo di rotazione necessario per puntare verso il target
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

            // Debug per controllare i valori calcolati
            Debug.Log($"Hand position: {handBone.position}, Target position: {target.position}");
            Debug.Log($"Direction: {direction}, Angle: {angle}");

            // Interpola la rotazione dell'osso della mano sinistra verso l'angolo calcolato
            float step = rotationSpeed * Time.deltaTime;
            float currentAngle = Mathf.LerpAngle(handBone.eulerAngles.z, angle, step);

            // Applica la rotazione
            handBone.rotation = Quaternion.Euler(new Vector3(0, 0, currentAngle));
        }
    }
}
