using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float moveSpeed = 2f; // Velocit� di movimento del nemico
    public float shootInterval = 3f; // Intervallo di tempo tra i colpi
    public GameObject projectilePrefab; // Prefab del proiettile
    public Transform shootPoint; // Punto di partenza del proiettile
    public float stopDuration = 1f; // Durata della pausa per sparare

    private Rigidbody2D rb;
    private float shootTimer;
    private bool isMoving = true;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        shootTimer = shootInterval;
    }

    void Update()
    {
        shootTimer -= Time.deltaTime;

        if (shootTimer <= 0)
        {
            Shoot();
            shootTimer = shootInterval + stopDuration; // Reset del timer con aggiunta della durata della pausa
        }

        if (isMoving)
        {
            Move();
        }
        else
        {
            Stop();
        }
    }

    void Move()
    {
        Vector2 movement = Vector2.left * moveSpeed * Time.deltaTime;
        rb.MovePosition(rb.position + movement);
    }

    void Stop()
    {
        // Il nemico � fermo
    }

    void Shoot()
    {
        isMoving = false;
        Instantiate(projectilePrefab, shootPoint.position, shootPoint.rotation);
        Invoke("ResumeMovement", stopDuration); // Riprende il movimento dopo la pausa
    }

    void ResumeMovement()
    {
        isMoving = true;
    }
}
