using UnityEngine;
using UnityEngine.SceneManagement;

public class NewLevel : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            UnlockNewLevel();
        }
    }

    void UnlockNewLevel()
    {
        // Ottieni l'indice dell'attuale livello (livello attuale)
        int currentLevel = SceneManager.GetActiveScene().buildIndex;

        // Ottieni il livello massimo sbloccato (di default 1)
        int unlockedLevel = PlayerPrefs.GetInt("UnlockedLevel", 1);

        // Sblocca il prossimo livello solo se l'attuale livello � maggiore o uguale a quello sbloccato
        if (currentLevel >= unlockedLevel)
        {
            // Sblocca il prossimo livello (incrementa di 1)
            PlayerPrefs.SetInt("UnlockedLevel", currentLevel + 1);

            // Salva i progressi
            PlayerPrefs.Save();

            Debug.Log("Nuovo livello sbloccato! Livello sbloccato: " + (currentLevel + 1));
        }
    }
}
