using UnityEngine;
using System.Collections;

public class FlameEnemyShooting : MonoBehaviour
{
    AudioManager audioManager;
    public GameObject bulletPrefab; // Prefab del proiettile
    public Transform firePoint; // Punto da cui vengono sparati i proiettili
    public float fireRate = 2f; // Tempo tra un colpo e l'altro in secondi

    public Transform target; // Riferimento al protagonista 'Gino'
    public float shootRadius = 5f; // Raggio d'azione per sparare, tunabile nell'Inspector

    public float stopDuration = 2f; // Durata del tempo di stop in secondi, tunabile nell'Inspector
    private bool _isStopped = false; // Flag per controllare se l'enemy � fermo
    public bool IsStopped { get { return _isStopped; } } // Propriet� pubblica per accedere al flag

    private bool isPushed = false; // Flag per controllare se l'enemy � spinto da una forza esterna

    private Rigidbody2D rb; // Riferimento al Rigidbody2D dell'enemy

    private float shootCounter = 0f; // Contatore per tracciare il tempo tra i colpi

    public GameObject exclamationMark;
    private float timePassing = 0f;
    public float bulletSpeed = 2f;
    public float decelerate = 0.5f;

    public float movementRadius = 1f; // Raggio del cerchio attorno al nemico in cui si muove il firePoint

    private SpriteRenderer spriteRenderer; // Riferimento al SpriteRenderer del nemico
    private bool isFlipped = false; // Stato attuale del flip della sprite

    // Aggiungi variabile per l'animazione
    public Animator animator; // Riferimento all'Animator

    void Start()
    {
        audioManager = GameObject.FindGameObjectWithTag("audio").GetComponent<AudioManager>();
        rb = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>(); // Ottieni il componente SpriteRenderer

        // Salva lo stato iniziale del flip della sprite
        isFlipped = spriteRenderer.flipX;
    }

    void Update()
    {
        // Incrementa il contatore di tempo
        shootCounter += Time.deltaTime;
        timePassing += Time.deltaTime;

        // Controlla se il nemico supera una velocit� orizzontale di 2
        if (Mathf.Abs(rb.velocity.x) > 2f)
        {
            OnPush();
        }

        // Controlla se il contatore ha raggiunto il valore di fireRate
        if (shootCounter >= fireRate && !isPushed)
        {
            StartCoroutine(StopEnemy());
        }

        if (timePassing > 0.5f)
        {
            timePassing = 0f;
        }

        // Controlla la distanza dal target e aggiorna l'animazione
        UpdateAnimationState();
    }

    void Shoot()
    {
        exclamationMark.SetActive(true);

        // Calcola la direzione verso il protagonista
        Vector2 direction = (target.position - firePoint.position).normalized;

        // Istanzia il proiettile e ottiene il suo Rigidbody2D
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Rigidbody2D bulletRb = bullet.GetComponent<Rigidbody2D>();

        // Imposta la velocit� iniziale del proiettile in base a bulletSpeed
        bulletRb.velocity = direction * bulletSpeed;

        // Inizia la decelerazione dopo il lancio
        flameBullet bulletScript = bullet.GetComponent<flameBullet>();
        if (bulletScript != null)
        {
            bulletScript.StartDeceleration();
        }

        exclamationMark.SetActive(false);
    }

    IEnumerator StopEnemy()
    {

        // Ferma l'enemy
        shootCounter = 0f;
        timePassing = 0;

        _isStopped = true;
        Vector2 originalVelocity = rb.velocity; // Salva la velocit� attuale
        rb.velocity = Vector2.zero; // Ferma il movimento
        // Riprendi il movimento
        rb.velocity = originalVelocity;
        if (Vector2.Distance(target.position, transform.position) <= shootRadius)
        {
            yield return new WaitForSeconds(stopDuration);
            Shoot();
            audioManager.PlaySFX(audioManager.flamethrower);
        }

        // Aspetta 0.2 secondi prima di ripristinare il flip della sprite
        yield return new WaitForSeconds(0.2f);

        // Ripristina lo stato originale del flip della sprite
        spriteRenderer.flipX = isFlipped;
        _isStopped = false;
    }

    // Metodo chiamato quando l'enemy viene colpito da una forza esterna
    public void OnPush()
    {
        isPushed = true;
        // Opzionalmente, puoi far tornare il flag a false dopo un certo tempo
        StartCoroutine(ResetPushFlag());
    }

    // Coroutine per resettare il flag isPushed dopo un certo tempo
    IEnumerator ResetPushFlag()
    {
        yield return new WaitForSeconds(stopDuration);
        isPushed = false;
    }

    // Metodo per aggiornare lo stato dell'animazione basato sulla distanza dal target
    void UpdateAnimationState()
    {
        // Calcola la distanza tra il nemico e il target
        float distance = Vector2.Distance(target.position, transform.position);
        bool isPlayerClose = distance <= shootRadius;

        // Debug: Mostra il valore di distance e la logica di isPlayerClose
        Debug.Log($"Distance to player: {distance}, shootRadius: {shootRadius}");
        Debug.Log($"isPlayerClose: {isPlayerClose}");

        // Aggiorna il parametro dell'animator
        if (animator != null)
        {
            animator.SetBool("isPlayerClose", isPlayerClose);

            // Debug: Mostra il valore del parametro dell'animator
            Debug.Log($"Animator Parameter 'isPlayerClose' set to: {animator.GetBool("isPlayerClose")}");
        }
        else
        {
            Debug.LogError("Animator component not assigned.");
        }
    }
}
