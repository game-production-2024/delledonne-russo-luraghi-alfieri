using UnityEngine;

public class deathZone : MonoBehaviour
{
    // Metodo chiamato quando un altro Collider2D entra in questo trigger collider
    private void OnTriggerEnter2D(Collider2D other)
    {
        // Controlla se l'oggetto che ha colliso ha il tag "nemico"
        if (other.CompareTag("nemico") || other.CompareTag("Player"))
        {
            // Distruggi l'oggetto che ha colliso
            Destroy(other.gameObject);
        }
    }
}
