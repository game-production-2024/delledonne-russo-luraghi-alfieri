using UnityEngine;
using UnityEngine.SceneManagement;

public class enemyManager : MonoBehaviour
{
     // Nome del livello successivo dopo che tutti i nemici sono stati distrutti

    public int enemyCount; // Conteggio degli oggetti "nemico" attivi
    public GameObject Arrowtoshow;

    void Start()
    {
        // Inizializza il conteggio dei nemici attivi
        enemyCount = GameObject.FindGameObjectsWithTag("nemico").Length;
        print(enemyCount);
    }

    void Update()
    {
        // Controlla se non ci sono pi� nemici attivi
        if (enemyCount <= 0)
        {
            // Se non ci sono pi� nemici, procedi al livello successivo
            LoadNextLevel();
        }
    }

    // Metodo per decrementare il conteggio dei nemici quando uno viene distrutto
    public void EnemyDestroyed()
    {
        enemyCount = enemyCount - 1;
        print(enemyCount);
    }

    // Metodo per caricare il livello successivo
    void LoadNextLevel()
    {

        // Sposta l'oggetto a destra
        transform.Translate(Vector3.right * 2f);
        Arrowtoshow.SetActive(true);
        // Modifica il valore 2f a seconda di quanto vuoi spostare l'oggetto

    }
}
