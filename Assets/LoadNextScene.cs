using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextSceneOnPlayerCollision : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Controlla se l'oggetto che ha colliso ha il tag "Player"
        if (collision.gameObject.CompareTag("Player"))
        {
            LoadNextSceneAndUnlock();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Controlla se l'oggetto che ha colliso ha il tag "Player"
        if (collision.gameObject.CompareTag("Player"))
        {
            LoadNextSceneAndUnlock();
        }
    }

    private void LoadNextSceneAndUnlock()
    {
        // Carica la scena successiva nell'indice delle scene
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;

        // Sblocca solo il livello successivo
        int unlockedLevel = PlayerPrefs.GetInt("Unlockedlevel", 1);
        if (unlockedLevel == currentSceneIndex + 1)
        {
            PlayerPrefs.SetInt("Unlockedlevel", nextSceneIndex);
            PlayerPrefs.Save();
        }

        // Carica la scena successiva
        if (nextSceneIndex < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(nextSceneIndex);
        }
        else
        {
            Debug.LogError("Indice del livello fuori dall'intervallo delle scene nel Build Settings!");
        }
    }
}
