using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    public AudioManager audioManager; // Riferimento all'AudioManager
    public Slider musicSlider; // Slider per il volume della musica
    public Slider sfxSlider; // Slider per il volume degli effetti sonori

    void Start()
    {
        // Imposta gli slider ai volumi correnti
        musicSlider.value = audioManager.GetMusicVolume();
        sfxSlider.value = audioManager.GetSfxVolume();

        // Aggiungi listener agli slider
        musicSlider.onValueChanged.AddListener(OnMusicSliderChange);
        sfxSlider.onValueChanged.AddListener(OnSfxSliderChange);
    }

    private void OnMusicSliderChange(float value)
    {
        audioManager.SetMusicVolume(value);
        Debug.Log($"Music Slider Changed: {value}"); // Debug: mostra il valore corrente dello slider
    }

    private void OnSfxSliderChange(float value)
    {
        audioManager.SetSfxVolume(value);
        Debug.Log($"SFX Slider Changed: {value}"); // Debug: mostra il valore corrente dello slider
    }
}
