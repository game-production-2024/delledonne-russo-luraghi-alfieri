using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    AudioSource audioSource;
    public float speed = 2f; // Velocità di movimento del nemico
    public bool movingRight = true; // Direzione del movimento
    private EnemyShooting enemyShooting; // Riferimento allo script di sparo
    private bool rotation = false;
    public Transform target;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        
        // Ottieni il riferimento allo script di sparo
        enemyShooting = GetComponent<EnemyShooting>();
    }

    // Update viene chiamato una volta per frame
    void Update()
    {
            if (!audioSource.isPlaying)
            {
                audioSource.Play();
            }
        // Controlla se il nemico è fermo
        if (enemyShooting != null && enemyShooting.IsStopped)
        {
            return; // Se il nemico è fermo, non eseguire il movimento
        }
        // Movimento del nemico
        if (movingRight)
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
            if (rotation == true)
            {
                transform.Rotate(0f, -180f, 0f);
                rotation = false;
            }

        }
        else
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
            if (rotation == true)
            {
                transform.Rotate(0f, 180f, 0f);
                rotation = false;
            }
        }
} 
    public void changeDirection(bool value)
    {
        movingRight = !movingRight;
        value = !value;
        rotation = true;
    }
    // Metodo per gestire le collisioni con i trigger
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Invertire la direzione del movimento
        changeDirection(true);
    }
}
