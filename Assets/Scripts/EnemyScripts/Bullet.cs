using UnityEngine;

public class Bullet : MonoBehaviour
{
    private void Start()
    {
            // Imposta il layer del proiettile
            gameObject.layer = LayerMask.NameToLayer("bullet");

            // Ottieni il LayerMask degli oggetti nemici
            int enemyLayer = LayerMask.NameToLayer("nemico");
            // Ignora le collisioni con il layer degli oggetti nemici
            Physics2D.IgnoreLayerCollision(gameObject.layer, enemyLayer);
            Physics2D.IgnoreLayerCollision(gameObject.layer, gameObject.layer);
    }
    // Metodo per gestire le collisioni
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Controlla se il proiettile ha toccato una piattaforma
        if (collision.gameObject.CompareTag("Ground") || collision.gameObject.CompareTag("Muro") || collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("nemico") || collision.gameObject.CompareTag("Box") || collision.gameObject.CompareTag("proiettile"))
        {
            Destroy(gameObject);    // Distrugge il proiettile
        }
    }
}
