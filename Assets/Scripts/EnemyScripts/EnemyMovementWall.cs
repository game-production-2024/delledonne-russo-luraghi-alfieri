using UnityEngine;

public enum GravityDirection
{
    Vertical,
    Horizontal
}

public class EnemyMovementWallAttach : MonoBehaviour
{
    public GravityDirection gravityDirection = GravityDirection.Vertical; // Direzione della gravit�
    public float speed = 2f; // Velocit� di movimento del nemico
    private bool movingRight = true; // Direzione del movimento

    private Rigidbody2D rb; // Riferimento al Rigidbody2D dell'enemy

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = 0f; // Disabilita la gravit� standard
    }

    void FixedUpdate()
    {
        // Applica la gravit� personalizzata manualmente
        ApplyCustomGravity();

        // Movimento del nemico
        if (movingRight)
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector2.left * speed * Time.deltaTime);
        }
    }

    // Applica la gravit� personalizzata in base alla direzione scelta
    private void ApplyCustomGravity()
    {
        switch (gravityDirection)
        {
            case GravityDirection.Vertical:
                rb.velocity += new Vector2(0f, -9.81f) * Time.fixedDeltaTime; // Gravit� verso il basso
                break;
            case GravityDirection.Horizontal:
                rb.velocity += new Vector2(-9.81f, 0f) * Time.fixedDeltaTime; // Gravit� verso sinistra
                break;
            default:
                break;
        }
    }

    // Metodo per gestire le collisioni con i trigger
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Invertire la direzione del movimento
        if (collision.CompareTag("Edge"))
        {
            movingRight = !movingRight;

            // Invertire la rotazione dell'asse y del nemico per farlo girare
            Vector3 currentRotation = transform.localEulerAngles;
            currentRotation.y += 180f; // Aggiungi 180 gradi alla rotazione sull'asse y
            transform.localEulerAngles = currentRotation;
        }
    }
}
