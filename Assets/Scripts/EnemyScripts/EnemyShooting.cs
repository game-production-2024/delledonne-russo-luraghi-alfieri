using UnityEngine;
using System.Collections;

public class EnemyShooting : MonoBehaviour
{
    AudioManager audioManager;
    public GameObject bulletPrefab; // Prefab del proiettile
    public Transform firePoint; // Punto da cui vengono sparati i proiettili
    public float fireRate = 2f; // Tempo tra un colpo e l'altro in secondi

    public Transform target; // Riferimento al protagonista 'Gino'

    public float stopDuration = 2f; // Durata del tempo di stop in secondi, tunabile nell'Inspector
    private bool _isStopped = false; // Flag per controllare se l'enemy è fermo
    public bool IsStopped { get { return _isStopped; } } // Proprietà pubblica per accedere al flag

    private bool isPushed = false; // Flag per controllare se l'enemy è spinto da una forza esterna

    private Rigidbody2D rb; // Riferimento al Rigidbody2D dell'enemy

    private float shootCounter = 0f; // Contatore per tracciare il tempo tra i colpi

    public GameObject exclamationMark;
    private bool isVisible = false;
    private float timePassing = 0f;

    private SpriteRenderer spriteRenderer; // Riferimento al SpriteRenderer del nemico

    void Start()
    {
        audioManager = GameObject.FindGameObjectWithTag("audio").GetComponent<AudioManager>();
        rb = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>(); // Ottieni il componente SpriteRenderer
    }

    void Update()
    {
        // Incrementa il contatore di tempo
        shootCounter += Time.deltaTime;
        timePassing += Time.deltaTime;

        // Controlla se il nemico supera una velocità orizzontale di 2
        if (Mathf.Abs(rb.velocity.x) > 2f)
        {
            OnPush();
        }

        // Controlla se il contatore ha raggiunto il valore di fireRate
        if (shootCounter >= fireRate && !isPushed)
        {
            StartCoroutine(StopEnemy());
        }

        if (isVisible == true && timePassing > 0.5f)
        {
            exclamationMark.SetActive(false);
            isVisible = false;
        }
    }

    void Shoot()
    {
        audioManager.PlaySFX(audioManager.robotShooting);
        isVisible = true;

        // Calcola la direzione verso il protagonista
        Vector2 direction = (target.position - firePoint.position).normalized;

        // Instanzia il proiettile e imposta la sua direzione
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);

        if (bullet != null)
        {
            // Ensure the bullet has a Rigidbody2D component
            Rigidbody2D bulletRb = bullet.GetComponent<Rigidbody2D>();
            if (bulletRb != null)
            {
                bulletRb.velocity = direction * 11f; // Velocità del proiettile
                Debug.Log("Proiettile sparato da: " + firePoint.position + " verso: " + direction);
            }
            else
            {
                Debug.LogError("Il proiettile non ha un componente Rigidbody2D");
            }
        }
        else
        {
            Debug.LogError("Errore nell'istanza del proiettile");
        }
}

IEnumerator StopEnemy()
    {
        // Ferma l'enemy
        shootCounter = 0f;
        timePassing = 0;
        exclamationMark.SetActive(true);
        _isStopped = true;
        Vector2 originalVelocity = rb.velocity; // Salva la velocità attuale
        rb.velocity = Vector2.zero; // Ferma il movimento

        // Aspetta per la durata specificata
        yield return new WaitForSeconds(stopDuration);

        // Spara il proiettile
        Shoot();

        // Aspetta 0.2 secondi prima di riprendere il movimento
        yield return new WaitForSeconds(0.2f);

        // Riprendi il movimento
        rb.velocity = originalVelocity;
        _isStopped = false;
    }

    // Metodo chiamato quando l'enemy viene colpito da una forza esterna
    public void OnPush()
    {
        isPushed = true;
        // Opzionalmente, puoi far tornare il flag a false dopo un certo tempo
        StartCoroutine(ResetPushFlag());
    }

    // Coroutine per resettare il flag isPushed dopo un certo tempo
    IEnumerator ResetPushFlag()
    {
        yield return new WaitForSeconds(stopDuration);
        isPushed = false;
    }
}
