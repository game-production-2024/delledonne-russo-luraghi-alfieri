using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    AudioManager audioManager;
    AudioSource audioSource;
    public ParticleSystem Dust;
    public TrailRenderer dashTrail;
    public KeyCode jumpKey = KeyCode.Space; // Il tasto di salto
    public float jumpForce = 8f; // La forza di salto
    public float maxJumpTime = 0.001f; // Durata massima del salto in secondi
    public float jumpHoldForce = 0.1f; // Forza aggiuntiva applicata durante il salto se il tasto � tenuto premuto
    public float maxFallSpeed = -1000f; // Velocit� massima di caduta consentita
    public float airControlMultiplier = 1f; // Moltiplicatore di controllo in aria
    public GameObject objectToActivateOnMousePress; // L'oggetto da attivare/disattivare con il tasto sinistro del mouse

    private SpriteRenderer spriteRenderer; // Renderer per cambiare la sprite
    private Sprite originalSprite; // La sprite originale del personaggio
    public GameObject hideWhileDashing;

    public float groundPoundForce = 20f; // La forza dello schianto verso il basso
    private bool isGroundPounding = false;
    private Rigidbody2D rb;
    private bool canJump = false;
    private bool isJumping = false;
    private bool isDashing = false;
    private bool hasDashedInAir = false; // Track if dash has been used in the air
    private float jumpTime = 0f;
    private float dashTime = 0f;
    private float dashCooldownTime = 0f; // Tempo trascorso dall'ultimo dash
    public float dashDuration = 0.2f; // Durata del dash in secondi
    public float dashCooldown = 0.5f; // Cooldown del dash in secondi

    public float meleeForwardPower = 10f; // Imposta un valore appropriato per la potenza del movimento in avanti

    public float DashForce = 13f; // Forza del dash
    public KeyCode dashKey = KeyCode.LeftShift;
    public float speed = 4.5f; // Velocit� di movimento del personaggio

    public GameObject objectToDeactivate; // Oggetto da disattivare durante il dash
    public GameObject objectToDeactivate2;
    // New variables for melee attack
    public KeyCode meleeButton = KeyCode.F; // Key for melee attack
    public GameObject[] groundPoundObjects;
    public GameObject[] meleeObjects; // Objects to activate during melee attack
    public float meleeDuration = 1f; // Duration for which melee objects stay active
    public float meleeCooldown = 1f; // Cooldown for melee attack
    public float meleeForwardSpeed = 5f; // Speed to move forward during melee

    private bool isMeleeActive = false; // Tracks if melee is active
    private float meleeTime = 0f; // Timer for melee duration
    private float meleeCooldownTime = 0f; // Timer for melee cooldown

    public float coyoteTime = 0.2f;
    private float coyoteTimeCounter = -0.1f;

    private GameOverManager gameOverManager; // Riferimento al GameOverManager
    float horizontalMove = 0f;
    public Animator animator;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioManager = GameObject.FindGameObjectWithTag("audio").GetComponent<AudioManager>();
        rb = GetComponent<Rigidbody2D>();
    spriteRenderer = GetComponent<SpriteRenderer>(); // Inizializza il componente SpriteRenderer
    originalSprite = spriteRenderer.sprite; // Memorizza la sprite originale
    gameOverManager = FindObjectOfType<GameOverManager>();
        if (gameOverManager == null)
        {
            Debug.LogError("GameOverManager non trovato nella scena.");
        }
    }

    void Update()
    {
        if (!canJump)
        {
            audioSource.enabled = false;
        }
        else
        {
            audioSource.enabled = true;
        }
        if (rb.velocity.x != 0)
        {
            if (!audioSource.isPlaying)
            {
                audioSource.Play();
            }
        }
        if (Input.GetKey(KeyCode.Mouse0))
        {
            
            if (objectToActivateOnMousePress != null)
            {
                SpriteRenderer spriteRenderer = objectToActivateOnMousePress.GetComponent<SpriteRenderer>();
                if (spriteRenderer != null)
                {
                    spriteRenderer.enabled = true; // Abilita il SpriteRenderer
                    animator.SetBool("isAiming", true);
                }
            }
        }
        else
        {
            if (objectToActivateOnMousePress != null)
            {
                SpriteRenderer spriteRenderer = objectToActivateOnMousePress.GetComponent<SpriteRenderer>();
                if (spriteRenderer != null)
                {
                    spriteRenderer.enabled = false; // Disabilita il SpriteRenderer
                    animator.SetBool("isAiming", false);
                }
            }
        }


        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));
        horizontalMove = Input.GetAxisRaw("Horizontal");

        if (canJump)
        {
            animator.SetBool("canJump", true);
            coyoteTimeCounter = coyoteTime;
        }
        else
        {
            animator.SetBool("canJump", false);
            coyoteTimeCounter -= Time.deltaTime;
        }

        if (isDashing)
        {
            objectToDeactivate.SetActive(false);
            objectToDeactivate2.SetActive(false);
            dashTrail.emitting = true;
            hideWhileDashing.gameObject.SetActive(false);
        }
        else
        {
            objectToDeactivate.SetActive(true);
            objectToDeactivate2.SetActive(true);
            dashTrail.emitting = false;
            hideWhileDashing.gameObject.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.LeftControl) && !isGroundPounding && !isMeleeActive && !canJump && !isDashing)
        {
            StartGroundPound();
            audioManager.PlaySFX(audioManager.groundpound);
        }

        // Flip character to face the direction of the cursor
        if (Input.GetKey(KeyCode.Mouse0))
        {

        
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (mousePosition.x > transform.position.x)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (mousePosition.x < transform.position.x)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        }
        // Movimento orizzontale
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float currentSpeed = speed;

        if (isMeleeActive)
        {
            animator.SetBool("isMelee", true);
            horizontalInput = 0f; // Disables horizontal input if melee is active
        }

        if (!canJump && rb.velocity.y < 0 && !isDashing)
        {
            currentSpeed *= airControlMultiplier; // Riduce la velocit� orizzontale in aria
        }

        if (!isDashing && !isMeleeActive)
        {
            rb.velocity = new Vector2(horizontalInput * currentSpeed, rb.velocity.y);
        }

        // Flip character to face the direction of movement
        if (horizontalInput > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (horizontalInput < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }

        // Salto
        if (coyoteTimeCounter > 0 && Input.GetKeyDown(jumpKey))
        {
            StartJump();
            audioManager.PlaySFX(audioManager.jump);
            coyoteTimeCounter = 0f;
            createJumpDust();
        }

        if (isJumping && Input.GetKey(jumpKey))
        {
            ContinueJump();
        }

        // Dash
        dashCooldownTime += Time.deltaTime; // Incrementa il tempo trascorso dall'ultimo dash

        if (dashCooldownTime >= dashCooldown && Input.GetKeyDown(dashKey) && !isDashing && horizontalInput != 0 && !isGroundPounding)
        {
            if (canJump || (!canJump && !hasDashedInAir))
            {
                StartDash(horizontalInput);
                audioManager.PlaySFX(audioManager.dash);
                animator.SetTrigger("DASH");
            }
        }

        // Resetta la possibilit� di dashare quando tocca terra
        if (canJump && !isDashing && dashCooldownTime >= dashCooldown)
        {
            hasDashedInAir = false; // Resets when touching the ground
        }

        // Melee attack
        meleeCooldownTime += Time.deltaTime;
        if (Input.GetKeyDown(meleeButton) && meleeCooldownTime >= meleeCooldown && !isGroundPounding)
        {
            StartMelee();
            animator.SetTrigger("MELEE");
        }

        // Melee timer
        if (isMeleeActive)
        {
            meleeTime += Time.deltaTime;

            if (meleeTime >= meleeDuration)
            {
                EndMelee();
            }
        }

        // Controlla se l'oggetto ha una velocit� verticale negativa
        if (rb != null && rb.velocity.y < 0)
        {
            animator.SetBool("isFalling", true);
        }
        else
        {
            animator.SetBool("isFalling", false);
        }

        if (rb != null && rb.velocity.y > 0)
        {
            animator.SetBool("isJumping", true);
        }
        else
        {
            animator.SetBool("isJumping", false);
        }
    }

    public void SetCanJump(bool value)
    {
        canJump = value;
    }

    void StartJump()
    {
        isJumping = true;
        jumpTime = 0f;
        rb.velocity = new Vector2(rb.velocity.x, 0f); // Resetta la velocit� verticale prima di saltare
        rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse); // Applica la forza di salto iniziale
    }

    void ContinueJump()
    {
        if (rb.velocity.y > 0 && jumpTime < maxJumpTime)
        {
            rb.AddForce(Vector2.up * jumpHoldForce, ForceMode2D.Force); // Applica la forza aggiuntiva mentre il tasto � tenuto premuto
            jumpTime += Time.deltaTime;
        }
        else
        {
            isJumping = false;
        }
    }

    void StartDash(float horizontalInput)
    {
        isDashing = true;
        animator.SetBool("isDashing", true);
        dashTime = 0f;
        dashCooldownTime = 0f; // Resetta il timer del cooldown

        // Determina la direzione del dash in base all'input orizzontale
        float dashDirection = Mathf.Sign(horizontalInput);

        // Applica la forza orizzontale per il dash
        rb.velocity = new Vector2(dashDirection * DashForce, 0f);

        if (!canJump)
        {
            hasDashedInAir = true; // Mark that dash has been used in the air
        }
    }

    void StartGroundPound()
    {
        isGroundPounding = true;
        animator.SetBool("isGroundPound", true);
        foreach (GameObject obj in groundPoundObjects)
        {
            obj.SetActive(true);
        }
        rb.velocity = Vector2.zero; // Resetta la velocit� per un GroundPound pi� controllato
        rb.AddForce(Vector2.down * groundPoundForce, ForceMode2D.Impulse);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        // Quando il personaggio tocca un oggetto, termina il GroundPound
        if (isGroundPounding)
        {
            EndGroundPound();
        }
    }

    void EndGroundPound()
    {
        isGroundPounding = false;
        animator.SetBool("isGroundPound", false);
        foreach (GameObject obj in groundPoundObjects)
        {
            obj.SetActive(false);
        }
    }

    void StartMelee()
    {
        audioManager.PlaySFX(audioManager.meleeAttack);
        isMeleeActive = true;
        meleeTime = 0f;
        meleeCooldownTime = 0f; // Resetta il timer del cooldown

        // Attiva gli oggetti dell'attacco melee
        foreach (GameObject obj in meleeObjects)
        {
            obj.SetActive(true);
        }

        // Muovi il personaggio in avanti durante l'attacco melee
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        if (rb != null)
        {
            Debug.Log("Applying forward force for melee attack.");

            // Verifica la scala X per determinare la direzione della forza
            float direction = transform.localScale.x < 0 ? -1 : 1;
            Vector2 forwardMovement = transform.right * meleeForwardPower * direction;
            rb.AddForce(forwardMovement, ForceMode2D.Impulse);
        }
        else
        {
            Debug.LogWarning("Rigidbody2D component not found on the character.");
        }
    }

    void EndMelee()
    {
        isMeleeActive = false;
        animator.SetBool("isMelee", false);
        foreach (GameObject obj in meleeObjects)
        {
            obj.SetActive(false);
        }
    }

    void FixedUpdate()
    {
        // Limita la velocit� di caduta massima
        if (rb.velocity.y < maxFallSpeed && !isDashing)
        {
            rb.velocity = new Vector2(rb.velocity.x, maxFallSpeed);
        }

        // Gestisce il dash
        if (isDashing)
        {
            dashTime += Time.fixedDeltaTime;

            // Mantieni la velocit� verticale a zero durante il dash
            rb.velocity = new Vector2(rb.velocity.x, 0f);

            if (dashTime >= dashDuration)
            {
                isDashing = false;
                animator.SetBool("isDashing", false);
            }
        }
    }

    // Metodo chiamato quando l'oggetto viene distrutto
    void OnDestroy()
    {
        if (gameOverManager != null)
        {
            gameOverManager.ShowRestartButton();
        }
    }

    void createJumpDust()
    {
        Dust.Play();
    }

    private bool AnimatorHasTrigger(string triggerName)
    {
        // Check if the Animator has the specified trigger
        foreach (AnimatorControllerParameter param in animator.parameters)
        {
            if (param.type == AnimatorControllerParameterType.Trigger && param.name == triggerName)
            {
                return true;
            }
        }
        return false;
    }
}



public class PlayerDeathController : MonoBehaviour
{
    private DeathCounter deathCounter;

    void Start()
    {
        deathCounter = FindObjectOfType<DeathCounter>();
        if (deathCounter == null)
        {
            Debug.LogError("DeathCounter not found in the scene!");
        }
    }

    void Update()
    {
        if (IsPlayerDead())
        {
            Debug.Log("Player is dead");
            deathCounter.IncrementDeathCount();
            HandlePlayerDeath();
        }
    }

    bool IsPlayerDead()
    {
        // Logica per determinare se il giocatore � morto
        // Questo � solo un esempio, dovresti sostituirlo con la tua logica
        return transform.position.y < -10;
    }

    void HandlePlayerDeath()
    {
        // Logica per gestire la morte del giocatore
        // Ad esempio, riposiziona il giocatore al punto di partenza
        transform.position = Vector3.zero;
    }
}