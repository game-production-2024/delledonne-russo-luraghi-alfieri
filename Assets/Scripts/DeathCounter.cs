using UnityEngine;
using UnityEngine.UI;

public class DeathCounter : MonoBehaviour
{
    public Text deathText;  // Riferimento al testo UI che mostrer� il numero di morti
    private int deathCount = 0;

    // Metodo per incrementare il contatore delle morti
    public void IncrementDeathCount()
    {
        deathCount++;
        Debug.Log("Death count incremented: " + deathCount);
        UpdateDeathText();
    }

    // Metodo per aggiornare il testo UI
    private void UpdateDeathText()
    {
        Debug.Log("Updating death text: " + deathCount);
        deathText.text = "Deaths: " + deathCount;
    }
}
