using System.Collections;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    AudioManager audioManager;
    // Lista di oggetti da attivare/disattivare
    public GameObject[] objectsToToggle;

    // Timer per attivare e disattivare gli oggetti
    public float activationTime = 2f; // Tempo durante il quale gli oggetti saranno attivi
    public float deactivationTime = 2f; // Tempo durante il quale gli oggetti saranno disattivati

    private void Start()
    {
        audioManager = GameObject.FindGameObjectWithTag("audio").GetComponent<AudioManager>();
        // Avvia la routine per attivare e disattivare gli oggetti
        StartCoroutine(ToggleObjectsRoutine());
    }

    private IEnumerator ToggleObjectsRoutine()
    {
        while (true)
        {
            // Attiva gli oggetti
            SetObjectsActive(true);
            audioManager.PlaySFX(audioManager.electricity);

            // Aspetta per il tempo definito
            yield return new WaitForSeconds(activationTime);

            // Disattiva gli oggetti
            SetObjectsActive(false);
            // Aspetta per il tempo definito
            yield return new WaitForSeconds(deactivationTime);
        }
    }

    // Funzione per attivare o disattivare gli oggetti
    private void SetObjectsActive(bool isActive)
    {
        foreach (GameObject obj in objectsToToggle)
        {
            obj.SetActive(isActive);
        }
    }
}
