using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boxCrushKill : MonoBehaviour
{
    private enemyManager enemyManager;
    private bool healthPoints = true;
    public GameObject objectToDestroy;
    // Start is called before the first frame update
    void Start()
    {
        enemyManager = GameObject.FindObjectOfType<enemyManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (healthPoints == false)
        {
            // Distrugge l'oggetto
            Destroy(gameObject);
        }
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Box") || collision.gameObject.CompareTag("Muro"))
        {
            healthPoints = false;
        }
    }
    private void OnDestroy()
    {
        enemyManager.EnemyDestroyed();
        Destroy(objectToDestroy);
    }
}
