using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speed = 20f;
    public string groundTag = "Ground";
    public string wallTag = "Wall";
    public string enemyTag = "nemico";
    public string playerTag = "Player";
    public bool isShot = false; // Variabile che indica se il proiettile � stato sparato

    private Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        if (isShot)
        {
            rb.velocity = transform.right * speed;
        }
            // Imposta il layer del proiettile
            gameObject.layer = LayerMask.NameToLayer("Proiettile");

            // Ottieni il LayerMask degli oggetti nemici
            int enemyLayer = LayerMask.NameToLayer("Player");

            // Ignora le collisioni con il layer degli oggetti nemici
            Physics2D.IgnoreLayerCollision(gameObject.layer, enemyLayer);
        }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isShot)
        {
            Destroy(gameObject);
        }
    }
}