using UnityEngine;

public class elenKill : MonoBehaviour
{
    public string bulletTag ="proiettile"; // Tag dell'oggetto bullet da rilevare
    public GameObject objectToDestroy;
    public GameObject objectToShow;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(bulletTag))
        {
            Destroy(objectToDestroy);
            objectToShow.SetActive(true);
            // Distrugge l'oggetto specificato se viene colpito da un bullet
        }

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Controlla se l'oggetto con cui � entrato in collisione ha il tag "Platform"
        if (collision.gameObject.CompareTag(bulletTag))
        {
            // Distrugge il proiettile
            Destroy(gameObject);
        }
    }
}