using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI;

public class flameEnemyMovement : MonoBehaviour
{
    public float speed = 2f; // Velocit� di movimento del nemico
    public bool movingRight = true; // Direzione del movimento
    private FlameEnemyShooting enemyShooting; // Riferimento allo script di sparo

    public flameEnemyMovement(FlameEnemyShooting enemyShooting)
    {
        this.enemyShooting = enemyShooting;
    }

    private bool rotation = false;
    public Transform target;
    private enemyManager enemyManager;

    void Start()
    {
        enemyManager = GameObject.FindObjectOfType<enemyManager>();
        // Ottieni il riferimento allo script di sparo
        enemyShooting = GetComponent<FlameEnemyShooting>();
    }

    // Update viene chiamato una volta per frame
    void Update()
    {
        // Controlla se il nemico � fermo
        if (enemyShooting != null && enemyShooting.IsStopped)
        {
            return; // Se il nemico � fermo, non eseguire il movimento
        }
        // Movimento del nemico
        if (movingRight)
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
            if (rotation == true)
            {
                transform.Rotate(0f, -180f, 0f);
                rotation = false;
            }

        }
        else
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
            if (rotation == true)
            {
                transform.Rotate(0f, 180f, 0f);
                rotation = false;
            }
        }
    }
    public void changeDirection(bool value)
    {
        movingRight = !movingRight;
        value = !value;
        rotation = true;
    }
    // Metodo per gestire le collisioni con i trigger
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Invertire la direzione del movimento
        changeDirection(true);
    }
    private void OnDestroy()
    {
        enemyManager.EnemyDestroyed();
    }

    public override bool Equals(object obj)
    {
        return obj is flameEnemyMovement movement &&
               base.Equals(obj) &&
               EqualityComparer<FlameEnemyShooting>.Default.Equals(enemyShooting, movement.enemyShooting);
    }
}
