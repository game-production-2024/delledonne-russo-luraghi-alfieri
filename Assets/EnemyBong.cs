using UnityEngine;

public class MoveOnCollision : MonoBehaviour
{
    // Valore regolabile nell'Inspector
    public float moveDistance = 1.0f;

    // Metodo chiamato quando questo GameObject entra in collisione con un altro collider 2D
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Controlla se il GameObject con cui abbiamo colliso ha il tag "Muro" o "Ground"
        if (collision.gameObject.CompareTag("Muro") || collision.gameObject.CompareTag("player"))
        {
            // Determina la direzione della collisione
            ContactPoint2D contact = collision.GetContact(0);
            Vector2 collisionDirection = contact.point - (Vector2)transform.position;

            // Inverte la direzione sull'asse X
            float moveDirection = collisionDirection.x > 0 ? -1 : 1;

            // Muove il GameObject lungo l'asse X nella direzione opposta alla collisione
            transform.position = new Vector3(transform.position.x + moveDistance * moveDirection, transform.position.y, transform.position.z);
        }
    }
}