using UnityEngine;

public class DestroynemicoOnContact : MonoBehaviour
{
    // Questo metodo viene chiamato quando un collider entra in collisione con questo oggetto
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Controlla se l'oggetto con cui � entrato in contatto ha il tag "Enemy"
        if (collision.gameObject.CompareTag("nemico") || collision.gameObject.CompareTag("Player"))
        {
            // Distruggi l'oggetto con il tag "Enemy"
            Destroy(collision.gameObject);

            // Messaggio di debug per indicare che l'enemy � stato distrutto
        }
    }
}
