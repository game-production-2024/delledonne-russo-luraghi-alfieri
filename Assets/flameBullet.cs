using Unity.VisualScripting;
using UnityEngine;

public class flameBullet : MonoBehaviour
{
    public float decelerationRate = 1f; // Tasso di decelerazione
    private bool isStopping = false;
    private Rigidbody2D rb;
    public float stopThreshold = 0.1f;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        // Imposta il layer del proiettile
        gameObject.layer = LayerMask.NameToLayer("Bullet");

        // Ottieni il LayerMask degli oggetti nemici
        int enemyLayer = LayerMask.NameToLayer("Nemico");

        // Ignora le collisioni con il layer degli oggetti nemici
        Physics2D.IgnoreLayerCollision(gameObject.layer, enemyLayer);
    }
    // Metodo per gestire le collisioni
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Controlla se il proiettile ha toccato una piattaforma
        if (collision.gameObject.CompareTag("Ground") || collision.gameObject.CompareTag("Muro") || collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("nemico") || collision.gameObject.CompareTag("Box") || collision.gameObject.CompareTag("proiettile"))
        {
            Destroy(gameObject);    // Distrugge il proiettile
        }
    }
    void Update()
    {
        if (!isStopping) return;

        // Riduce gradualmente la velocit� del proiettile
        if (rb.velocity.magnitude > stopThreshold)
        {
            rb.velocity = rb.velocity * (1f - decelerationRate * Time.deltaTime);
        }
        else
        {
            rb.velocity = Vector2.zero;
            Destroy(gameObject); // Distrugge il proiettile quando si ferma
        }
    }

    // Metodo per iniziare il rallentamento
    public void StartDeceleration()
    {
        isStopping = true;
    }
}