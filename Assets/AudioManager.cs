using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [Header("-------- Audio Source ----------")]
    [SerializeField] AudioSource musicSource;
    [SerializeField] AudioSource SFXSource;

    [Header("-------- Audio Clip ----------")]
    public AudioClip bigRobotHurt;
    public AudioClip bowShooting;
    public AudioClip boxMoving;
    public AudioClip electricity;
    public AudioClip flamethrower;
    public AudioClip groundpound;
    public AudioClip meleeAttack;
    public AudioClip miniRobotHurt;
    public AudioClip enemyFootsteps;
    public AudioClip dash;
    public AudioClip jump;
    public AudioClip playerFootsteps;
    public AudioClip plankBreaking;
    public AudioClip backgroundMusic;
    public AudioClip robotShooting;

    public static AudioManager Instance;

    private Dictionary<AudioClip, AudioSource> activeSounds = new Dictionary<AudioClip, AudioSource>();
    private float musicVolume = 0.5f; // Valore fisso per test
    private float sfxVolume = 0.5f; // Valore fisso per test

    private void Awake()
    {
        // Singleton pattern implementation
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject); // Prevents the object from being destroyed when changing scenes
        }
        else
        {
            Destroy(gameObject); // Destroys duplicate AudioManager instances
        }
    }

    private void Start()
    {
        // Riproduci la musica di sottofondo
        musicSource.clip = backgroundMusic;
        musicSource.volume = musicVolume; // Imposta il volume iniziale della musica
        musicSource.Play();

        // Imposta il volume iniziale degli effetti sonori
        SFXSource.volume = sfxVolume;

        // Debug: verifica i volumi iniziali
        Debug.Log($"Initial Music Volume: {musicSource.volume}");
        Debug.Log($"Initial SFX Volume: {SFXSource.volume}");
    }

    public void SetMusicVolume(float volume)
    {
        musicVolume = Mathf.Clamp01(volume); // Assicurati che il valore sia compreso tra 0 e 1
        musicSource.volume = musicVolume; // Imposta il volume dell'AudioSource
        Debug.Log($"Music Volume Set to: {musicVolume}"); // Debug: verifica il valore impostato
    }

    public void SetSfxVolume(float volume)
    {
        sfxVolume = Mathf.Clamp01(volume); // Assicurati che il valore sia compreso tra 0 e 1
        SFXSource.volume = sfxVolume; // Imposta il volume dell'AudioSource
        Debug.Log($"SFX Volume Set to: {sfxVolume}"); // Debug: verifica il valore impostato
    }

    public float GetMusicVolume() // Assicurati di avere un metodo per ottenere il volume
    {
        return musicVolume;
    }

    public float GetSfxVolume() // Assicurati di avere un metodo per ottenere il volume
    {
        return sfxVolume;
    }

    public void PlaySFX(AudioClip clip)
    {
        // Controlla se il suono � gi� in riproduzione
        if (activeSounds.ContainsKey(clip))
        {
            if (!activeSounds[clip].isPlaying)
            {
                // Se l'audio � finito, rimuovilo e riproduci nuovamente
                activeSounds.Remove(clip);
                PlayNewSFX(clip);
            }
            else
            {
                Debug.Log("Il suono " + clip.name + " � gi� in riproduzione.");
            }
        }
        else
        {
            // Se il suono non � in riproduzione, riproducilo
            PlayNewSFX(clip);
        }
    }

    // Funzione per riprodurre il nuovo SFX
    private void PlayNewSFX(AudioClip clip)
    {
        // Usa un nuovo AudioSource temporaneo per questo suono
        AudioSource newSource = gameObject.AddComponent<AudioSource>();
        newSource.clip = clip;
        newSource.volume = sfxVolume; // Imposta il volume dell'effetto sonoro
        newSource.Play();

        // Aggiungi il suono al dizionario degli attivi
        activeSounds.Add(clip, newSource);

        // Avvia la coroutine per rimuovere l'AudioSource una volta che il suono � finito
        StartCoroutine(RemoveAudioSourceWhenFinished(newSource, clip));
    }

    // Coroutine per rimuovere l'AudioSource quando ha finito di riprodurre il suono
    private IEnumerator RemoveAudioSourceWhenFinished(AudioSource source, AudioClip clip)
    {
        yield return new WaitWhile(() => source.isPlaying);
        activeSounds.Remove(clip);  // Rimuovi il suono dal dizionario quando termina
        Destroy(source);  // Distruggi l'AudioSource temporaneo
    }
}
