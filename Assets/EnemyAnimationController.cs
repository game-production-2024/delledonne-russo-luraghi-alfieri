using UnityEngine;

public class EnemyAnimationController : MonoBehaviour
{
    public Animator animator; // Assegna l'Animator tramite l'Inspector

    private void Start()
    {
        // Assicurati che l'animazione di idle sia impostata come predefinita
        if (animator == null)
        {
            Debug.LogError("Animator non assegnato!");
        }
    }

    // Metodo da chiamare quando il nemico inizia a sparare
    public void StartSparare()
    {
        animator.SetTrigger("Sparare");
    }

    // Metodo da chiamare quando il nemico torna in idle
    public void StopSparare()
    {
        // Se hai bisogno di ripristinare lo stato di idle in modo esplicito
        // puoi usare questo metodo se necessario
    }
}
