using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class breakableFloor : MonoBehaviour
{
    AudioManager audioManager;

    private void Start()
    {
        audioManager = GameObject.FindGameObjectWithTag("audio").GetComponent<AudioManager>();
    }
    // Start is called before the first frame update
    void OnTriggerEnter2D(Collider2D collision)
    {
        // Controlla se l'oggetto che ha toccato il trigger ha il tag "groundPound"
        if (collision.gameObject.CompareTag("groundPound"))
        {
            audioManager.PlaySFX(audioManager.plankBreaking);
            // Distrugge il GameObject a cui � attaccato questo script
            Destroy(gameObject);
        }
    }
}
